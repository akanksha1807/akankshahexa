public class callstack
{
    public static void main(String args[])
    {
        System.out.println("executed before method1");
        method1();
        System.out.println("executed after method1");
    }
    public static void method1()
    {
        System.out.println("executed before method2");
        method2();
        System.out.println("executed after method2");
    }
    public static void method2()
    {
        System.out.println("executed before method3");
        method3();
        System.out.println("executed after method3");
    }
    public static void method3()
    {
        System.out.println("executed before method3");}
}
